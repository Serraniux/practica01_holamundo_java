package com.example.holamundo_01_java_s;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText nombreUser;
    TextView Salud0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void  saludar(View v){

        nombreUser=findViewById(R.id.txtNombre);
        Salud0=findViewById(R.id.lbSaludo);

        //Toast.makeText(this, "Hola "+nombreUser.getText().toString() ,Toast.LENGTH_LONG);
        if (nombreUser.getText().toString().matches("")){
            Toast.makeText(this, "Falto capturar un nombre",Toast.LENGTH_SHORT).show();
        }
        else{
            Salud0.setText("Hola "+nombreUser.getText().toString());
        }
    }
    public void limpiar(View v){
        if (nombreUser.getText().toString().matches("") && Salud0.getText().toString().matches("")){
            Toast.makeText(this, "No hay nada escrito",Toast.LENGTH_SHORT).show();
        }
        else{
            Salud0.setText("");
            nombreUser.setText("");
        }
    }
    public void Fin(View v){
        this.finish();
    }
}